@extends('layouts.master');

@section('content')
	<h3 class="text-center">Change Password</h3>
	<form action="/changepassword" method="POST">
		@csrf
	    <div class = "col-lg-4 offset-lg-4 p-5" style="box-shadow: 0px 0px 3px gray;">

            <div class="form-group">
                <label for="old_password">Old Password:</label>
                <div class="input-group mb-3">
                    <input type ="password" name="old_password" class="form-control border border-dark rounded-0 pwd"/>
                    <div class="input-group-prepend">
                        <span class="input-group-text reveal" id="basic-addon1"><i class="fas fa-eye"></i></span>
                     </div>
                 </div>
            </div>

            <div class="form-group">
                <label for="password">New Password</label>

                <div class="input-group mb-3">
                    <input type="password" name="password" id="password" class="form-control border border-dark rounded-0 pwd1"/>
                    <div class="input-group-prepend">
                        <span class="input-group-text reveal1" id="basic-addon1"><i class="fas fa-eye"></i></span>
                     </div>
                 </div>
            </div>

            <div class="form-group">
                <label for="password_confirmation">Retype Password</label>
                <div class="input-group mb-3">
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control border border-dark rounded-0 pwd2">
                    <div class="input-group-prepend">
                        <span  toggle="#password-field" class="input-group-text reveal2" id="basic-addon1"><i class="fas fa-eye"></i></span>
                     </div>
                 </div>
            </div>

	        <input type="submit" value="Submit" class="btn btn-outline-success rounded-0 text-uppercase font-weight-bold btn-block">
	    </div>
	</form>

@endsection

@section('script')

	<script>

        $(".reveal").on('click',function() {
            var $pwd = $(".pwd");
            if ($pwd.attr('type') === 'password') {
                $pwd.attr('type', 'text');
            } else {
                $pwd.attr('type', 'password');
            }
        });

        $(".reveal1").on('click',function() {
            var $pwd = $(".pwd1");
            if ($pwd.attr('type') === 'password') {
                $pwd.attr('type', 'text');
            } else {
                $pwd.attr('type', 'password');

            }
        });

        $(".reveal2").on('click',function() {
            var $pwd = $(".pwd2");
            if ($pwd.attr('type') === 'password') {
                $pwd.attr('type', 'text');
            } else {
                $pwd.attr('type', 'password');
            }
        });
	</script>

@endsection
